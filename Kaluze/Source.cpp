#include <iostream>
#include <queue>
#include <vector>
#include <fstream>
using namespace std;

struct punkt {
	int x;
	int y;
	int wysokosc;

	punkt(int x, int y, int wysokosc) {
		this->x = x;
		this->y = y;
		this->wysokosc = wysokosc;
	}

	punkt() {

	}
};

//struktura uzywana w liscie zawierajaca wskaznik na nastepny element
struct kontener {
	kontener *nastepny;
	punkt zawartosc;
};
class ListaJednokierunkowa {
public:
	kontener *poczatekListy;
	kontener *koniecListy;
	ListaJednokierunkowa();

	bool jestPusta();
	void dodajNaKoniec(punkt dane);
	void wypisz();
	void zeruj();
	punkt usunPoczatek();
	int rozmiar;
};
ListaJednokierunkowa::ListaJednokierunkowa() {
	poczatekListy = NULL;
	koniecListy = NULL;
	rozmiar = 0;
}
bool ListaJednokierunkowa::jestPusta() {
	if (poczatekListy == NULL) {
		return true;
	}
	return false;
}
void ListaJednokierunkowa::dodajNaKoniec(punkt dane) {
	//tworzenie elementu ktory chcesz dodac
	kontener *nowyElement = new kontener();
	nowyElement->zawartosc = dane;

	//jestli lista jest pusta to bedzie piewszy element
	if (jestPusta()) {
		nowyElement->nastepny = NULL;

		poczatekListy = nowyElement;
		koniecListy = nowyElement;
	}
	//w przeciwnym przypadku dodajesz element na koniec
	else {
		koniecListy->nastepny = nowyElement;
		nowyElement->nastepny = NULL;
		koniecListy = nowyElement;
	}
	rozmiar++;
}
void ListaJednokierunkowa::zeruj() {
	//jestli jest co usuwac
	if (poczatekListy != NULL) {
		//tworzysz wskaznik na element ktory usuwasz i element nastepny zeby nie stracic dowiazania do listy
		kontener *tmp = poczatekListy;
		kontener *tmp2 = poczatekListy->nastepny;

		//usuwasz od przodu az dojdziesz do ostatniego elementu
		while (tmp != koniecListy)
		{
			delete tmp;
			tmp = tmp2;
			tmp2 = tmp2->nastepny;
		}
		//potem usuwasz i ostatnio oraz ustawiasz na null poczatkowy i koncowy wskaznik tak samo jak w konstruktorze
		delete tmp;
		poczatekListy = NULL;
		koniecListy = NULL;
	}
	rozmiar = 0;
}
punkt ListaJednokierunkowa::usunPoczatek() {
	punkt zwrot;
	if (poczatekListy != NULL) {
		//tworzysz wskaznik na element ktory usuwasz i element nastepny zeby nie stracic dowiazania do listy
		kontener *tmp = poczatekListy;
		kontener *tmp2 = poczatekListy->nastepny;

		zwrot = tmp->zawartosc;
		delete tmp;
		poczatekListy = tmp2;
	}
	rozmiar--;
	return zwrot;
}


//Deklaracja klasy kopca
class kopiec {
public:
	punkt *tablica;
	int rozmiar;
	kopiec(int a);
	void dodaj(punkt v);
	void usun_korzen();
	void wyswietl(int poczatek);
	punkt zwroc_minimum();
};

//Konstruktor - tworzy pusty kopiec
kopiec::kopiec(int a) {
	rozmiar = 0;
	tablica = new punkt[a];
}

//Wstawia element o wartosci v do kopca
void kopiec::dodaj(punkt v) {
	tablica[rozmiar + 1] = v;
	int s = rozmiar + 1;
	while (s != 1) {
		if (tablica[s / 2].wysokosc > tablica[s].wysokosc) {
			swap(tablica[s / 2], tablica[s]); s /= 2;
		}
		else
			break;
	}
	rozmiar++;
}

//Usuwa korzen kopca
void kopiec::usun_korzen() {
	tablica[1] = tablica[rozmiar];
	rozmiar--;
	int tmp = 1;
	while (tmp * 2 <= rozmiar) {
		if (tablica[tmp].wysokosc > tablica[tmp * 2].wysokosc || tablica[tmp].wysokosc > tablica[tmp * 2 + 1].wysokosc) {
			if (tablica[tmp * 2].wysokosc < tablica[tmp * 2 + 1].wysokosc || tmp * 2 + 1 > rozmiar) {
				swap(tablica[tmp], tablica[tmp * 2]); tmp = tmp * 2;
			}
			else {
				swap(tablica[tmp], tablica[tmp * 2 + 1]); tmp = tmp * 2 + 1;
			}
		}
		else
			break;
	}
}

//Wyswietla zawartosc kopca poczawszy od elementu poczatek
void kopiec::wyswietl(int poczatek) {
	if (poczatek <= rozmiar) {
		cout << poczatek << " : " << tablica[poczatek].wysokosc << " \n";
		if (poczatek * 2 <= rozmiar) wyswietl(poczatek * 2);
		if (poczatek * 2 + 1 <= rozmiar) wyswietl(poczatek * 2 + 1);
	}
}

//zwraca minimum czyli korzen 
punkt kopiec::zwroc_minimum() {
	punkt zwrot = tablica[1];
	usun_korzen();
	return zwrot;
}

void wypisz(int **mapa, int maxX, int maxY) {
	for (int i = 0; i < maxX; i++)
	{
		for (int j = 0; j < maxY; j++) {
			cout << mapa[i][j] << " ";
		}
		cout << endl;
	}
}
void wypisz(bool **kaluze, int maxX, int maxY) {
	for (int i = 0; i < maxX; i++)
	{
		for (int j = 0; j < maxY; j++) {
			if (kaluze[i][j] == false)
				cout << ".";
			else
				cout << "#";
		}
		cout << endl;
	}

}

//szukanie wzniesienia ktore jest sasiadem punktu, ma wieksza wartosc od tego punktu i jednoczesnie jest najmniejsza wartoscia sposrod wszystkich sasiadow ale nie bylo odwiedzone
int najmniejszeWzniesienie(int **mapa, bool **odwiedzone, int minimum, int x, int y, int wysokosc)
{
	//int minimum = 2147483647;
	//ta wartosc jest najwieksza wartoscia INT, wiec to znaczy ze kazda inna wartosc bedzie mniejsza wiec od razu sie nadpisze jesli bedzie spelniac ponizsze warunki

	if (mapa[x - 1][y] > wysokosc && odwiedzone[x - 1][y] == false)
		if (minimum > mapa[x - 1][y])
			minimum = mapa[x - 1][y];

	if (mapa[x + 1][y] > wysokosc && odwiedzone[x + 1][y] == false)
		if (minimum > mapa[x + 1][y])
			minimum = mapa[x + 1][y];

	if (mapa[x][y - 1] > wysokosc && odwiedzone[x][y - 1] == false)
		if (minimum > mapa[x][y - 1])
			minimum = mapa[x][y - 1];

	if (mapa[x][y + 1] > wysokosc && odwiedzone[x][y + 1] == false)
		if (minimum > mapa[x][y + 1])
			minimum = mapa[x][y + 1];

	return minimum;
}

//rekurencyjna funkcja ktora idzie przez caly "lej" i dodaje go do tablicy odplywy
void przepisz(bool **odwiedzone, bool **odplywy, int x, int y)
{
	//jesli punkt w ktorym jestem to nie odplyw sprawdz mu sasiadow
	if (odplywy[x][y] == false && odwiedzone[x][y] == true)
	{
		odplywy[x][y] = true;
		przepisz(odwiedzone, odplywy, x - 1, y);
		przepisz(odwiedzone, odplywy, x + 1, y);
		przepisz(odwiedzone, odplywy, x, y - 1);
		przepisz(odwiedzone, odplywy, x, y + 1);
	}
	//czyszczenie tablicy odwiedzone
	odwiedzone[x][y] = false;


	//ListaJednokierunkowa Q;
	//Q.dodajNaKoniec(punkt(x, y, x));

	//punkt aktualny;
	//while (Q.rozmiar != 0) {
	//	aktualny = Q.usunPoczatek();
	//	if (odwiedzone[aktualny.x - 1][aktualny.y] == true && odplywy[aktualny.x - 1][aktualny.y] == false)
	//		Q.dodajNaKoniec(punkt(aktualny.x - 1, aktualny.y, x));
	//	if (odwiedzone[aktualny.x + 1][aktualny.y] == true && odplywy[aktualny.x + 1][aktualny.y] == false)
	//		Q.dodajNaKoniec(punkt(aktualny.x + 1, aktualny.y, x));
	//	if (odwiedzone[aktualny.x][aktualny.y - 1] == true && odplywy[aktualny.x][aktualny.y - 1] == false)
	//		Q.dodajNaKoniec(punkt(aktualny.x, aktualny.y - 1, x));
	//	if (odwiedzone[aktualny.x][aktualny.y + 1] == true && odplywy[aktualny.x][aktualny.y + 1] == false)
	//		Q.dodajNaKoniec(punkt(aktualny.x, aktualny.y + 1, x));

	//	odwiedzone[aktualny.x][aktualny.y] = false;
	//	odplywy[aktualny.x][aktualny.y] = true;

	//	//kaluze[aktualny.x][aktualny.y] = minimum - mapa[aktualny.x][aktualny.y];
	//}
	//return;
}

//prawie taka sama funkcja co wyzej tylko ze w tablicy kaluze wpisuje glebokosc kaluz
void sumuj(int **mapa, bool **odwiedzone, int **kaluze, int x, int y, int minimum)
{
	if (odwiedzone[x][y] == true)
	{
		//czyszczenie tablicy odwiedzone
		odwiedzone[x][y] = false;

		//wpisanie glebokosci kaluzy
		kaluze[x][y] = minimum - mapa[x][y];

		sumuj(mapa, odwiedzone, kaluze, x - 1, y, minimum);
		sumuj(mapa, odwiedzone, kaluze, x + 1, y, minimum);
		sumuj(mapa, odwiedzone, kaluze, x, y - 1, minimum);
		sumuj(mapa, odwiedzone, kaluze, x, y + 1, minimum);
	}
	return;

	//
	//
	//ListaJednokierunkowa Q;
	//	Q.dodajNaKoniec(punkt(x, y, minimum));
	//
	//	punkt aktualny;
	//	while (Q.rozmiar != 0) {
	//		aktualny = Q.usunPoczatek();
	//		if (odwiedzone[aktualny.x - 1][aktualny.y] == true)
	//			Q.dodajNaKoniec(punkt(aktualny.x - 1, aktualny.y, minimum));
	//		if (odwiedzone[aktualny.x + 1][aktualny.y] == true)
	//			Q.dodajNaKoniec(punkt(aktualny.x + 1, aktualny.y, minimum));
	//		if (odwiedzone[aktualny.x][aktualny.y - 1] == true)
	//			Q.dodajNaKoniec(punkt(aktualny.x, aktualny.y - 1, minimum));
	//		if (odwiedzone[aktualny.x][aktualny.y + 1] == true)
	//			Q.dodajNaKoniec(punkt(aktualny.x, aktualny.y + 1, minimum));
	//
	//		odwiedzone[aktualny.x][aktualny.y] = false;
	//
	//		kaluze[aktualny.x][aktualny.y] = minimum - mapa[aktualny.x][aktualny.y];
	//	}
	//	return;
}

void algorytm(kopiec heap, int **mapa, int **kaluze, int maxX, int maxY) {
	//ten program polega na tym aby zaczac od najmniejszych wierzcholkow, zeby zawsze zaczynac od najmniejszych wierzcholkow wszystkie byly dodane do kopca
	//jak juz jestes w najmniejszym wierzcholku musisz przejsc przez wszystkie sasiednie wierzcholki ktore sa mniejsze odkladajac je w kolejce
	//i sprawdzic co sie stanie, jesli dojdziesz do odplywu, to znaczy ze woda sie wyleje
	//jesli nie dojdziesz do odplywu to znaczy ze jest to kaluza i musisz ja zapisac
	//slad ktory tworzysz przy przechodzeniu po mniejszych lub rownych sasiadach nazywam lejem w komentarzach
	//brzmi prosto jestnak nie jest takie proste do zrobienia

	//kolejka w ktorym rozpatrywany jest obecny lej
	//vector <punkt> Q;
	ListaJednokierunkowa Q;

	//tworzenie tablicy odplywow
	bool **odplywy = new bool *[maxX];
	for (int x = 0; x < maxX; ++x) {
		odplywy[x] = new bool[maxY]();
	}

	//tworzenie tablicy odwiedzonych podczas przeszukiwania sasiadow czyli odwiedzone w danym leju
	bool **odwiedzone = new bool *[maxX];
	for (int x = 0; x < maxX; ++x) {
		odwiedzone[x] = new bool[maxY]();
	}

	//dodawanie ramki do odplywow, jak wiesz na brzegach woda zawsze sie wyleje wiec kazdy brzeg jest odplywem
	for (int x = 0; x < maxX; ++x) {
		odplywy[x][0] = true;
		odplywy[x][maxY - 1] = true;
	}

	for (int y = 0; y < maxY; ++y) {
		odplywy[0][y] = true;
		odplywy[maxX - 1][y] = true;
	}

	//zmienne uzywane w petli while
	int minimum;
	punkt aktualny;
	punkt startowy;


	while (heap.rozmiar != 0)
	{
		minimum = 2147483647;
		startowy = heap.zwroc_minimum();

		//cout << "wyruszam z: [" << startowy.x << "][" << startowy.y << "] = " << startowy.wysokosc << endl;

		//dodanie do kolejki elementu z kopca
		//Q.push_back(startowy);
		Q.dodajNaKoniec(startowy);

		while (1) {
			//biore element z kolejki
			//aktualny = Q.front();
			//Q.erase(Q.begin());
			aktualny = Q.usunPoczatek();

			//oznaczam ten element jako LOKALNIE odwiedzony
			odwiedzone[aktualny.x][aktualny.y] = true;

			//cout << "ROZPATRUJE: [" << aktualny.x << "][" << aktualny.y << "] = " << aktualny.wysokosc << endl;

			//jesli punkt nalezy do odplywu usuwam wszystkie punkty ktore mialem jeszcze rozpatrzec ( bo juz bez sensu je rozpatrywac
			if (odplywy[aktualny.x][aktualny.y] == true) {
				//cout << "punkt nalezy do odplywu" << endl;
				//cout << "musze usunac wszystkie z kolejki do rozpatrywania" << endl;

				//while (Q.size() != 0)
				while (Q.rozmiar != 0)
				{
					//punkt tmp = Q.front();
					//Q.pop_back();
					//punkt tmp = Q.usunPoczatek();
					Q.zeruj();

					//cout << "tmp[" << tmp.x << "][" << tmp.y << "]=" << tmp.wysokosc << endl;
				}
				//tutaj wszystkie punkty z rozpatrywanego "leja" trafiaja do tablicy odplywow czyli poszerzaja stopniowo ramke z ktorej splywa woda
				przepisz(odwiedzone, odplywy, startowy.x, startowy.y);

				//cout << "teraz odplywy wygladaja tak" << endl;
				//wypisz(odplywy, maxX, maxY);
				//cout << "a odwiedzone wygladaja tak" << endl;
				//wypisz(odwiedzone, maxX, maxY);
				break;
			}

			//szukanie najnizszego punktu wyzszego od naszego 
			//minimum = najmniejszeWzniesienie(mapa, odwiedzone, minimum, aktualny.x, aktualny.y, startowy.wysokosc);
			//_________________________________________________________________________________________
			//cout << "minimalny sasiad: " << minimum << endl;
			//_________________________________________________________________________________________


			if (odwiedzone[aktualny.x - 1][aktualny.y] == false) {
				if (mapa[aktualny.x - 1][aktualny.y] <= startowy.wysokosc) {
					//Q.push_back(punkt(aktualny.x - 1, aktualny.y, mapa[aktualny.x - 1][aktualny.y]));
					Q.dodajNaKoniec(punkt(aktualny.x - 1, aktualny.y, mapa[aktualny.x - 1][aktualny.y]));
					odwiedzone[aktualny.x - 1][aktualny.y] = true;
					//cout << "dodaje do kolejki";
					//cout << aktualny.x - 1 << "][" << aktualny.y << "] = " << mapa[aktualny.x - 1][aktualny.y] << endl;
				}
				else if (minimum > mapa[aktualny.x - 1][aktualny.y])
					minimum = mapa[aktualny.x - 1][aktualny.y];
			}
			if (odwiedzone[aktualny.x + 1][aktualny.y] == false) {
				if (mapa[aktualny.x + 1][aktualny.y] <= startowy.wysokosc) {
					//Q.push_back(punkt(aktualny.x + 1, aktualny.y, mapa[aktualny.x + 1][aktualny.y]));
					Q.dodajNaKoniec(punkt(aktualny.x + 1, aktualny.y, mapa[aktualny.x + 1][aktualny.y]));
					odwiedzone[aktualny.x + 1][aktualny.y] = true;
					//cout << "dodaje do kolejki";
					//cout << aktualny.x + 1 << "][" << aktualny.y << "] = " << mapa[aktualny.x + 1][aktualny.y] << endl;
				}
				else if (minimum > mapa[aktualny.x + 1][aktualny.y])
					minimum = mapa[aktualny.x + 1][aktualny.y];
			}
			if (odwiedzone[aktualny.x][aktualny.y - 1] == false) {
				if (mapa[aktualny.x][aktualny.y - 1] <= startowy.wysokosc) {
					//Q.push_back(punkt(aktualny.x, aktualny.y - 1, mapa[aktualny.x][aktualny.y - 1]));
					Q.dodajNaKoniec(punkt(aktualny.x, aktualny.y - 1, mapa[aktualny.x][aktualny.y - 1]));
					odwiedzone[aktualny.x][aktualny.y - 1] = true;
					//cout << "dodaje do kolejki";
					//cout << aktualny.x << "][" << aktualny.y - 1 << "] = " << mapa[aktualny.x][aktualny.y - 1] << endl;
				}
				else if (minimum > mapa[aktualny.x][aktualny.y - 1])
					minimum = mapa[aktualny.x][aktualny.y - 1];
			}

			if (odwiedzone[aktualny.x][aktualny.y + 1] == false) {
				if (mapa[aktualny.x][aktualny.y + 1] <= startowy.wysokosc) {
					//Q.push_back(punkt(aktualny.x, aktualny.y + 1, mapa[aktualny.x][aktualny.y + 1]));
					Q.dodajNaKoniec(punkt(aktualny.x, aktualny.y + 1, mapa[aktualny.x][aktualny.y + 1]));
					odwiedzone[aktualny.x][aktualny.y + 1] = true;
					//cout << "dodaje do kolejki";
					//cout << aktualny.x << "][" << aktualny.y + 1 << "] = " << mapa[aktualny.x][aktualny.y + 1] << endl;
				}
				else if (minimum > mapa[aktualny.x][aktualny.y + 1])
					minimum = mapa[aktualny.x][aktualny.y + 1];
			}


			////ETAP 2 czyli dodawanie do kolejki mniejszych sasiadow do przetworzenia
			//if (mapa[aktualny.x - 1][aktualny.y] <= startowy.wysokosc && odwiedzone[aktualny.x - 1][aktualny.y] == false) {
			//	//Q.push_back(punkt(aktualny.x - 1, aktualny.y, mapa[aktualny.x - 1][aktualny.y]));
			//	Q.dodajNaKoniec(punkt(aktualny.x - 1, aktualny.y, mapa[aktualny.x - 1][aktualny.y]));
			//	odwiedzone[aktualny.x - 1][aktualny.y] = true;
			//	//cout << "dodaje do kolejki";
			//	//cout << aktualny.x - 1 << "][" << aktualny.y << "] = " << mapa[aktualny.x - 1][aktualny.y] << endl;
			//}
			//if (mapa[aktualny.x + 1][aktualny.y] <= startowy.wysokosc && odwiedzone[aktualny.x + 1][aktualny.y] == false) {
			//	//Q.push_back(punkt(aktualny.x + 1, aktualny.y, mapa[aktualny.x + 1][aktualny.y]));
			//	Q.dodajNaKoniec(punkt(aktualny.x + 1, aktualny.y, mapa[aktualny.x + 1][aktualny.y]));
			//	odwiedzone[aktualny.x + 1][aktualny.y] = true;
			//	//cout << "dodaje do kolejki";
			//	//cout << aktualny.x + 1 << "][" << aktualny.y << "] = " << mapa[aktualny.x + 1][aktualny.y] << endl;
			//}
			//if (mapa[aktualny.x][aktualny.y - 1] <= startowy.wysokosc && odwiedzone[aktualny.x][aktualny.y - 1] == false) {
			//	//Q.push_back(punkt(aktualny.x, aktualny.y - 1, mapa[aktualny.x][aktualny.y - 1]));
			//	Q.dodajNaKoniec(punkt(aktualny.x, aktualny.y - 1, mapa[aktualny.x][aktualny.y - 1]));
			//	odwiedzone[aktualny.x][aktualny.y - 1] = true;
			//	//cout << "dodaje do kolejki";
			//	//cout << aktualny.x << "][" << aktualny.y - 1 << "] = " << mapa[aktualny.x][aktualny.y - 1] << endl;
			//}
			//if (mapa[aktualny.x][aktualny.y + 1] <= startowy.wysokosc && odwiedzone[aktualny.x][aktualny.y + 1] == false) {
			//	//Q.push_back(punkt(aktualny.x, aktualny.y + 1, mapa[aktualny.x][aktualny.y + 1]));
			//	Q.dodajNaKoniec(punkt(aktualny.x, aktualny.y + 1, mapa[aktualny.x][aktualny.y + 1]));
			//	odwiedzone[aktualny.x][aktualny.y + 1] = true;
			//	//cout << "dodaje do kolejki";
			//	//cout << aktualny.x << "][" << aktualny.y + 1 << "] = " << mapa[aktualny.x][aktualny.y + 1] << endl;
			//}

			//jesli nie dodalem do kolejki zadnego mniejszego sasiada to znaczy ze "lej" tutaj sie konczy
			if (Q.rozmiar == 0)
			{
				//cout << " w tym momencie powinienem zsumowac wszystko w kolejce !" << endl;
				//int dummyZmienna = sumuj(mapa, odwiedzone, kaluze, startowy.x, startowy.y, minimum);
				//sumaWody += dummyZmienna;
				sumuj(mapa, odwiedzone, kaluze, startowy.x, startowy.y, minimum);

				//cout << " z tego wierzcholka jest tyle wody: " << dummyZmienna << endl;

				//sumaWody += sumuj(mapa, odwiedzone, kaluze, startowy.x, startowy.y, minimum);


				//cout << "teraz KALUZE wygladaja tak" << endl;
				//wypisz(kaluze, maxX, maxY);
				//cout << "a MAPA wygladaja tak" << endl;
				//wypisz(mapa, maxX, maxY);
				break;
			}
		}
	}
}

int main()
{
	//--------------------- odkomentowujac ten bloczek  tekstu wlaczysz odczyt z pliku
	std::ifstream in("InAndOutFiles/8.in");
	std::streambuf *cinbuf = std::cin.rdbuf(); //save old buf
	std::cin.rdbuf(in.rdbuf()); //redirect std::cin to in.txt!

								//std::ofstream out("9.txt");
								//std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
								//std::cout.rdbuf(out.rdbuf()); //redirect std::cout to out.txt!
								//------------------------------------
	int maxX, maxY;

	cin >> maxX >> maxY;

	//tworzenie kopca
	kopiec heap(maxX * maxY + 1);

	//tworzenie tablic 2d
	int **mapa = new int *[maxX];
	int **kaluze = new int *[maxX];

	for (int x = 0; x < maxX; ++x) {
		mapa[x] = new int[maxY];
		kaluze[x] = new int[maxY]();
	}

	//dodawanie kazdego punktu mapy do kopca
	for (int x = 0; x < maxX; ++x) {
		for (int y = 0; y < maxY; ++y) {
			cin >> mapa[x][y];
			if (x > 0 && x < maxX - 1 && y > 0 && y < maxY - 1) heap.dodaj(punkt(x, y, mapa[x][y]));
		}
	}
	//wywolanie glownej funkcji
	algorytm(heap, mapa, kaluze, maxX, maxY);

	//zsumowanie objetosci wszystkich kaluz
	int wynik = 0;
	for (int i = 0; i < maxX; i++)
	{
		for (int j = 0; j < maxY; j++) {
			wynik += kaluze[i][j];
		}
	}

	//wypisanie wszystkich kaluz
	cout << wynik << endl;
	for (int i = 0; i < maxX; i++)
	{
		for (int j = 0; j < maxY; j++) {
			if (kaluze[i][j] == 0)
				cout << ".";
			else
				cout << "#";
		}
		cout << endl;
	}

	system("pause");
	return 0;
}